import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity, TextInput } from 'react-native';
import styles from './Styles/ButtonStyles';
import ExamplesRegistry from '../Services/ExamplesRegistry';

ExamplesRegistry.addComponentExample('Button', () =>
  <DrawerButton
    text='Button'
    onPress={() => window.alert('Your drawers are showing')}
  />
)

class Button extends Component {
  static propTypes = {
    text: PropTypes.string,
    onPress: PropTypes.func
  }

  render() {
    const { onPressAction, text } = this.props;
    return (
      <TouchableOpacity style={styles.button} onPress={onPressAction}>
        <Text style={styles.buttonText}>{text.toUpperCase()}</Text>
      </TouchableOpacity>
    )
  }
}

export default Button;
