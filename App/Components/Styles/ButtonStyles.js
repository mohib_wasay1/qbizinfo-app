import { Metrics, Colors, Fonts } from '../../Themes';

export default {
  button: {
    backgroundColor: '#fff',
    borderRadius: 30,
    width: 350,
    height: 50,
    marginBottom: 10
  },
  buttonText: {
    textAlign: 'center',
    padding: 12,
    fontSize: 18,
    fontWeight: 'bold',
    color: '#444',
  }
};
