import { Metrics, Colors, Fonts } from '../../Themes';

export default {
  textInput: {
    width: 350,
    height: 50,
    color: '#fff',
    borderWidth: 1,
    borderColor: '#fff',
    padding: 13,
    borderRadius: 30,
    paddingHorizontal: 20,
    fontSize: 14,
    marginBottom: 20
  }
};
