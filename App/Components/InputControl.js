import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity, TextInput } from 'react-native';
import styles from './Styles/InputControlStyles';
import ExamplesRegistry from '../Services/ExamplesRegistry';

ExamplesRegistry.addComponentExample('Input Control', () =>
  <DrawerButton
    text='Input Control'
    onPress={() => window.alert('Your drawers are showing')}
  />
)

class InputControl extends Component {
  static propTypes = {
    text: PropTypes.string,
    onPress: PropTypes.func
  }

  render() {
    const { onChange, value, placeholder } = this.props;
    return (
      <TextInput
        style={styles.textInput}
        onChangeText={onChange}
        placeholder={placeholder}
        placeholderTextColor='white'
        underlineColorAndroid="transparent"
        value={value}
      />
    )
  }
}

export default InputControl;
