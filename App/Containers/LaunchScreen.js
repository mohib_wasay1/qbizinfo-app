import React, { Component } from 'react'

import {
  ScrollView,
  Text,
  Image,
  View
} from 'react-native';

import { Images } from '../Themes';

import Button from '../Components/Button';

// Styles
import styles from './Styles/LaunchScreenStyles';

export default class LaunchScreen extends Component {
  onPress() {
    this.props.navigation.navigate('LoginScreen');
  }
  render() {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.launch} style={styles.logo} />
            <View style={{ padding: 50 }}></View>
            <Button onPressAction={() => this.onPress()} text={'Getting Started'} />
          </View>
        </ScrollView>
      </View>
    );
  }
}
