import React, { Component } from 'react';
import { ScrollView, Text, Image, View, TouchableOpacity } from 'react-native';
import { Images } from '../Themes';

import Input from '../Components/InputControl';
import Button from '../Components/Button';

import styles from './Styles/SignupScreenStyles';

export default class SignupScreen extends Component {
  state = { text: 'Useless Placeholder' };

  render() {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.launch} style={styles.logo} />
            <View style={{ padding: 20 }}></View>
            <Input value={this.state} placeholder={'First Name'} />
            <Input value={this.state} placeholder={'Last Name'} />
            <Input value={this.state} placeholder={'Username'} />
            <Input value={this.state} placeholder={'Email'} />
            <Input value={this.state} placeholder={'Password'} />
            <Input value={this.state} placeholder={'Confirm Password'} />
            <Button onPressAction={() => this.onPress()} text={'Sign In'} />
          </View>
        </ScrollView>
      </View>
    )
  }
}
