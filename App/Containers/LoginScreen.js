import React, { Component } from 'react';
import { ScrollView, Text, Image, View, TouchableOpacity } from 'react-native';
import { Images } from '../Themes';

import Input from '../Components/InputControl';
import Button from '../Components/Button';

import styles from './Styles/LoginScreenStyles';

export default class LoginScreen extends Component {
  state = { text: 'Useless Placeholder' };

  onPress() {
    this.props.navigation.navigate('SignupScreen');
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.launch} style={styles.logo} />
            <View style={{ padding: 20 }}></View>
            <Input value={this.state} placeholder={'Username'} />
            <Input value={this.state} placeholder={'Password'} />
            <Button onPressAction={() => this.onPress()} text={'Sign In'} />
            <Button onPressAction={() => this.onPress()} text={'Create new Account'} />
            <Button onPressAction={() => this.onPress()} text={'Signin with LinkedIn'} />
          </View>
        </ScrollView>
      </View>
    )
  }
}
